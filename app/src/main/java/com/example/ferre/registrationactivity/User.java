package com.example.ferre.registrationactivity;

import java.io.Serializable;

/**
 * Created by Ferre on 13/02/2017.
 */

public class User implements Serializable {
    private String login;
    private String password;
    private String naam;

    public User(String login, String password, String naam)
    {
        this.login = login;
        this.password = password;
        this.naam = naam;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setNaam(String naam)
    {
        this.naam = naam;
    }

    public String getLogin ()
    {
        return login;
    }

    public String getPassword()
    {
        return password;
    }

    public String getNaam()
    {
        return naam;
    }


}
