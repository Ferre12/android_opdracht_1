package com.example.ferre.registrationactivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static android.R.attr.password;
import static com.example.ferre.registrationactivity.R.id.nameEditText;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //register
        final Button registerButton = (Button) findViewById(R.id.addUserButton);
        registerButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent registerIntent = new Intent();

                EditText loginEditText = (EditText) findViewById(R.id.loginEditText);
                EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);
                EditText confirmPasswordEditText = (EditText) findViewById(R.id.confirmPasswordEditText);
                EditText nameEditText = (EditText) findViewById(R.id.nameEditText);

                String passwordText = passwordEditText.getText().toString().trim();
                String confirmPasswordText = confirmPasswordEditText.getText().toString().trim();

                if (passwordText.equals(confirmPasswordText)) {
                    TextView lblError = (TextView) findViewById(R.id.lblEror);
                    lblError.setVisibility(View.INVISIBLE);
                    User newUser = createUser(loginEditText.getText().toString(), passwordEditText.getText().toString(), nameEditText.getText().toString());
                    registerIntent.putExtra("user",newUser);

                    setResult(Activity.RESULT_OK, registerIntent);
                    finish();
                }
                else
                {
                    TextView lblError = (TextView) findViewById(R.id.lblEror);
                    lblError.setText("Passwords don't match!");
                    lblError.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public User createUser(String login, String password, String name)
    {
        return new User(login,password,name);
    }
}
