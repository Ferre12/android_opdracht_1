package com.example.ferre.registrationactivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private ArrayList<User> users;
    public final static int MESSAGE_REQUEST_CODE = 0;

    public MainActivity()
    {


        super();
        users = new ArrayList<>();
        users.add(new User("login1", "password1", "name1"));
        users.add(new User("login2", "password2", "name2"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //register
        Button registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent registerIntent = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivityForResult(registerIntent, MESSAGE_REQUEST_CODE);

            }
        });

        //login
        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String login = ((EditText)findViewById(R.id.editText)).getText().toString();
                String password = ((EditText) findViewById(R.id.editText2)).getText().toString();

                for(User u: users)
                {
                    if(u.getLogin().equals(login))
                    {
                        if(u.getPassword().equals(password))
                        {
                            Intent userIntent = new Intent(getApplicationContext(),UserActivity.class);
                            userIntent.putExtra("user",u);

                            if(userIntent.resolveActivity(getPackageManager()) != null)
                            {
                                startActivity(userIntent);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == MESSAGE_REQUEST_CODE)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                User user = (User) data.getSerializableExtra("user");
                users.add(user);

                //all users
                for(User u: users)
                {
                    Log.d("Name", u.getNaam());
                    Log.d("Login", u.getLogin());
                    Log.d("Password", u.getPassword());
                }

            }
        }
    }
}
