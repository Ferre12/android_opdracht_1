package com.example.ferre.registrationactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.Serializable;

public class UserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        String name = "no data from intent";
        Intent intent = getIntent();
        if (intent != null)
        {
            if(intent.hasExtra("user"))
            {
                User user = (User) intent.getSerializableExtra("user");
                name = user.getNaam();
            }
        }

        TextView tv = (TextView) findViewById(R.id.textView8);
        tv.setText(name);
    }
}
